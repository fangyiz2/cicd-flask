from flask import Flask, jsonify, request
from flask_restful import Resource, Api, reqparse
from pymongo import MongoClient

import query
import scraping
from flask_cors import CORS

app = Flask(__name__)
CORS(app)


def get_db():
    client = MongoClient('localhost', 27017)
    exist_db = client['good_reads']
    return exist_db


def get_id_and_col(item):
    item_id = int(request.args.get('id'))
    exist_db = get_db()
    collection = exist_db.get_collection(item + "_info")
    return item_id, collection


@app.route('/api/book', methods=['GET'])
def get_book():
    book_id, book_mongodb = get_id_and_col("book")
    target_book = book_mongodb.find_one({"_id": book_id})
    if target_book is None:
        return "No such id is found!"
    return jsonify(target_book)


@app.route('/api/book', methods=['PUT'])
def put_book():
    book_id, book_mongodb = get_id_and_col("book")
    target_book = book_mongodb.find_one({"_id": book_id})
    js_content = request.get_json(force=True)
    if type(js_content) is list:
        return "Json file is mal-structured!"
    for k, v in js_content.items():
        book_mongodb.update_one({k: target_book[k]}, {'$set': {k: v}})
    return "Successfully put!"


@app.route('/api/book', methods=['POST'])
def post_book():
    book_id, book_mongodb = get_id_and_col("book")
    js_content = request.get_json(force=True)
    if type(js_content) is list:
        return "Json file is mal-structured!"
    book_mongodb.insert_one(js_content)
    return "Successfully posted!"


@app.route('/api/book', methods=['DELETE'])
def del_book():
    book_id, book_mongodb = get_id_and_col("book")
    book_mongodb.delete_one({"_id": book_id})
    return "Successfully deleted!"


@app.route('/api/books', methods=['POST'])
def post_books():
    exist_db = get_db()
    book_mongodb = exist_db.get_collection("book_info")
    js_content = request.get_json(force=True)
    for book in js_content:
        book_mongodb.insert_one(book)
    return 'Successfully posted!'


@app.route('/api/author', methods=['GET'])
def get_author():
    author_id, author_mongodb = get_id_and_col("author")
    target_author = author_mongodb.find_one({"_id": author_id})
    if target_author is None:
        return "No such id is found!"
    return jsonify(target_author)


@app.route('/api/author', methods=['PUT'])
def put_author():
    author_id, author_mongodb = get_id_and_col("author")
    target_author = author_mongodb.find_one({"_id": author_id})
    js_content = request.get_json(force=True)
    if type(js_content) is list:
        return "Json file is mal-structured!"
    for k, v in js_content.iteritems():
        author_mongodb.update_one({k: target_author[k]}, {'$set': {k: v}})
    return "Successfully put!"


@app.route('/api/author', methods=['POST'])
def post_author():
    author_id, author_mongodb = get_id_and_col("author")
    js_content = request.get_json(force=True)
    if type(js_content) is list:
        return "Json file is mal-structured!"
    author_mongodb.insert_one(js_content)
    return "Successfully posted!"


@app.route('/api/author', methods=['DELETE'])
def del_author():
    author_id, author_mongodb = get_id_and_col("author")
    author_mongodb.delete_one({"_id": author_id})
    return "Successfully deleted!"


@app.route('/api/authors', methods=['POST'])
def post_authors():
    exist_db = get_db()
    author_mongodb = exist_db.get_collection("author_info")
    js_content = request.get_json(force=True)
    for author in js_content:
        author_mongodb.insert_one(author)
    return 'Successfully posted!'


@app.route('/api/scrape', methods=['POST'])
def post_scrape():
    exist_db = get_db()
    attr = request.args.get('attr')
    url = "https://www.goodreads.com/" + attr
    if "book" in attr:
        scraping.scrape_and_store(exist_db, url, 1, 0)
        return "Successfully posted!"
    if "author" in attr:
        author = scraping.scrape_a_auth_url(url)
        scraping.scraping_related_author(author)
        author_mongodb = exist_db.get_collection("author_info")
        author_mongodb.insert_one(author)
        return "Successfully posted!"


@app.route('/api/search', methods=['GET'])
def search():
    query_str = str(request.args.get('q'))
    exist_db = get_db()
    collection_name = query.split('.')[0] + "_" + "info"
    data_col = exist_db.get_collection(collection_name)
    if "." not in query or ":" not in query:
        print_mal_query()
    if "AND" in query_str:
        target_obj = query.logic_operator("AND", query_str, "$and")
        return jsonify(target_obj)
    if "OR" in query_str:
        target_obj = query.logic_operator("OR", query_str, "$or")
        return jsonify(target_obj)
    if ":" in query_str:
        obj = data_col.find_one(query.contain_operator(query_str))
        return jsonify(obj)


def print_mal_query():
    print("The input is a malformed query string!")


if __name__ == '__main__':
    app.run(debug=True)
